<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Route::get('/dashboard', "App\Http\Controllers\ProductsController@index")
    ->middleware(['auth'])
    ->name('dashboard');

Route::post('/shopping-cart', "App\Http\Controllers\ProductsController@addProductToCart")
    ->middleware(['auth'])
    ->name('add-to-cart');

require __DIR__.'/auth.php';
