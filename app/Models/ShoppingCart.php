<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShoppingCart extends Model
{
    use HasFactory;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function products()
    {
        // TODO: Shopping Cart should have many to many relation with Products
        // Documentation: https://laravel.com/docs/8.x/eloquent-relationships
        // TIP: dont forget about migrations: https://laravel.com/docs/8.x/migrations
        throw new Exception("TODO: Shopping Cart should have many to many relation with Products");
    }
}
