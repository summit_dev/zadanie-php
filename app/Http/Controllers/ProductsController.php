<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class ProductsController extends Controller
{
    public function index(Request $request)
    {
        $products = Product::all();

        // TODO: get content of current shopping cart

        return view('products', compact('products'));
    }

    public function addProductToCart(Request $request)
    {
        throw new Exception("TODO: Save product to cart");
        // TODO: save product to cart
        return redirect()->back();
    }
}
