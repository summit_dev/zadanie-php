# Zadanie rekrutacyjne na stanowisko programisty PHP

Celem zadania jest sprawdzenie znajomości podstawowych zagadnień na jakie można trafić w trakcie codziennej pracy.

Dotyczy to między innymi:

1. Podstawowych zagadnień zwiazanych z PHP takich jak sesja, MVC czy z protokołem HTTP
2. Zagadnień komunikacji z bazą danch
3. Korzystania z dokumentacji
4. Podstawowych zagadnień bezpieczeństwa aplikacji
5. Rozwiązywania problemów napotkanych w trakcie pracy z nieznanym kodem

## Przygotowanie projektu

1. Sforkuj to repozytorium kodu
2. Pobierz kod
3. Zainstaluj zależności Composerem
4. Zainstaluj zależności NPM
5. Zbuduj frontend przy pomocy polecenia `npm run dev`
6. Uruchom lokalnie bazę danych (pomiń ten krok, jeśli chcesz użyć sqlite)
7. Utwórz plik .env w głównym katalogu aplikacji kopiując plik .env.example
8. W pliku .env zmień wartość APP_URL na zgodną z lokalnym URL aplikacji
9. Dodaj konfigurację bazy danych (https://laravel.com/docs/8.x/database)
10. Zmigruj bazę danych przy użyciu polecenia `php artisan migrate --seed`


## Uruchomienie aplikacji

Po Uruchomieniu aplikacji zaloguj się korzystając z loginu i hasła:

- test@example.com 
- zaq1@WSX

## Cel zadania

Celem zadania jest dodanie uproszczonej funkcjonalności koszyka.
Wymagania:

1. Zawartość koszyka powinna znajdować się w bazie danych (początkowa migracja bazy utworzyła tabele shopping_carts oraz products)
2. Id bieżącego koszyka powinno być zapisywane w sesji użytkownika
3. Uzytkownik powinien mieć możliwość podglądu produktów z koszyka na stronie głównej
4. Użytkownik powinien mieć możliwość dodawania produktów do koszyka
5. Użytkownik powinien mieć możliwość usuwania produktów z koszyka

## Informacje dla osób nieposiadających doświadczenia z frameworkiem laravel

1. Laravel korzysta z template engine blade - nie jest wymagana jego znajomość (aczkolwiek podstawy z dokumentacji byłyby plusem)
2. Plik HTML z którego trzeba korzystać znajduje się w folderze `resources/views/products.blade.php`
3. Miejsca, które wymagają uwagi są oznaczone wpisami TODO
4. Kontrolery znajdują się w folderze `app/Http/Controllers`
5. Routing definiuje się w pliku `routes/web.php` (w przypadku konieczności dodania nowej akcji można zrobić to na podstawie którejść z już dostępnych)
6. Definicje baz danych znajdują się w formie migracji w folderze `database/migrations`
7. Jeśli struktura bazy danych została zmieniona ręcznie należy tam umieścić plik SQL z poleceniami DDL
