<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::create([
            'name' => 'User',
            'email' => 'test@example.com',
            'email_verified_at' => now(),
            'password' => \Hash::make('zaq1@WSX'),
            'remember_token' => \Str::random(10),
        ]);

        $productNames = [
            "Bread",
            "Water",
            "Onions",
            "Eggs",
            "Sugar",
            "Chicken",
            "Book",
            "Toothbrush",
            "Orange juice",
            "Spinach",
            "Cucumber",
        ];

        foreach ($productNames as $name) {
            \App\Models\Product::create(['name' => $name]);
        }

        \App\Models\Product::create([
            'name' => '<script>alert("TODO: you should NOT see this message!")</script>'
        ]);
    }
}
